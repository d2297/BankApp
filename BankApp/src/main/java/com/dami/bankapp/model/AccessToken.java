package com.dami.bankapp.model;

import lombok.Data;

@Data
public class AccessToken {

    private final String accessToken;

}
