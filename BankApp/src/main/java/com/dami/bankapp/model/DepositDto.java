package com.dami.bankapp.model;

import lombok.Data;

@Data
public class DepositDto {

    private String accountNumber;
    private Double amount;

}
