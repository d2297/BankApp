package com.dami.bankapp.model;

import lombok.Data;

@Data
public class AccountInfo {
    private String accountName;
    private String accountNumber;
    private Double balance;
}
