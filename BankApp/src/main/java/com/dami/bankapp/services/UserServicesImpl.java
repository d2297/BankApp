package com.dami.bankapp.services;

import com.dami.bankapp.exceptions.BankAppException;
import com.dami.bankapp.model.AccessToken;
import com.dami.bankapp.model.Account;
import com.dami.bankapp.model.LoginDto;
import com.dami.bankapp.security.TokenServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserServicesImpl implements UserServices{

    @Autowired
    private BankServices bankServices;

    @Autowired
    private TokenServices tokenServices;

    @Autowired
    private MyUserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public AccessToken login(LoginDto loginDto) {
        Account account = bankServices.findAccountByAccountNumber(loginDto.getAccountNumber());
        if(!passwordEncoder.matches(loginDto.getPassword(), account.getPassword())){
            throw new BankAppException("Invalid password");
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(loginDto.getAccountNumber());
        String token = tokenServices.generateToken(userDetails);
        return new AccessToken(token);

    }


}
