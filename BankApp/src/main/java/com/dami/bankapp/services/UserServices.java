package com.dami.bankapp.services;

import com.dami.bankapp.model.AccessToken;
import com.dami.bankapp.model.LoginDto;
import org.springframework.stereotype.Service;

@Service
public interface UserServices {

    AccessToken login(LoginDto loginDto);
}
