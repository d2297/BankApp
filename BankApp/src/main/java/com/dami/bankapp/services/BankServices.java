package com.dami.bankapp.services;

import com.dami.bankapp.exceptions.BankAppException;
import com.dami.bankapp.model.Account;
import com.dami.bankapp.model.AccountDto;
import com.dami.bankapp.model.AccountInfo;
import com.dami.bankapp.model.Transaction;

import java.util.List;


public interface BankServices {
    String createAccount(AccountDto accountDto) throws BankAppException;

    Account findAccountByAccountName(String accountName) throws BankAppException;

    Account findAccountByAccountNumber(String accountNumber) throws BankAppException;

    String deposit(String accountNumber, Double amount) throws BankAppException;

    String withdraw(String accountNumber, String password, Double amount) throws BankAppException;

    AccountInfo getAccountInfo(String accountNumber, String accountPassword);

    List<Transaction> generateAccountStatement(String accountNumber, String accountPassword);


}
