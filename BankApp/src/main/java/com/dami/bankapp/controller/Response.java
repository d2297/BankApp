package com.dami.bankapp.controller;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class Response {
    private HttpStatus responseCode;
    private boolean success;
    private String message;



    public Response(HttpStatus responseCode, boolean success, String message) {
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
    }
}
