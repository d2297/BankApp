package com.dami.bankapp.controller;

import com.dami.bankapp.model.AccountInfo;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class AccountInfoApiResponse {

    private HttpStatus responseCode;
    private boolean success;
    private String message;
    private AccountInfo account;

    public AccountInfoApiResponse(HttpStatus responseCode, boolean success, String message, AccountInfo account) {
        this.responseCode = responseCode;
        this.success = success;
        this.message = message;
        this.account = account;
    }
}
