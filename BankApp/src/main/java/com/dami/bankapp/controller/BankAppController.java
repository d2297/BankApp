package com.dami.bankapp.controller;

import com.dami.bankapp.exceptions.BankAppException;
import com.dami.bankapp.model.*;
import com.dami.bankapp.services.BankServices;
import com.dami.bankapp.services.UserServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequestMapping("/bank-app")
public class BankAppController {

    @Autowired
    BankServices bankServices;

    @Autowired
    UserServices userServices;

    @PostMapping("/register")
    public Response register(@RequestBody AccountDto accountDto){

        String message = null;
        try {
            message = bankServices.createAccount(accountDto);
            return new Response(HttpStatus.OK, true, message);
        } catch (BankAppException e) {
            return new Response(HttpStatus.BAD_REQUEST, false, e.getMessage());
        }
    }

    @PostMapping("/login")
    public Response login(@RequestBody LoginDto loginDto) {
        String token;

        try {
            bankServices.findAccountByAccountNumber(loginDto.getAccountNumber());
            token = userServices.login(loginDto).getAccessToken();
        } catch (BankAppException e) {
            return new Response(HttpStatus.BAD_REQUEST, false, e.getMessage());
        }
        return new Response(HttpStatus.OK, true, token);


    }

    @PostMapping("/deposit")
    public Response deposit(@RequestBody DepositDto depositDto) { String accountInfo;
        try {
            log.info("account number from deposit endpoint is ---->{}", depositDto.getAccountNumber());
            accountInfo = bankServices.deposit(depositDto.getAccountNumber(), depositDto.getAmount());

        } catch (BankAppException exception) {
            return new Response(HttpStatus.BAD_REQUEST, false, exception.getMessage());
        }
        return new Response(HttpStatus.OK, true, accountInfo);

    }

    @PostMapping("/withdraw")
    public Response withdrawal(@RequestBody WithdrawalDto withdrawalDto) {
        String accountInfo;
        try {
            accountInfo = bankServices.withdraw(withdrawalDto.getAccountNumber(), withdrawalDto.getPassword(), withdrawalDto.getAmount());

        } catch (BankAppException exception) {
            return new Response(HttpStatus.BAD_REQUEST, false, exception.getMessage());
        }
        return new Response(HttpStatus.OK, true, accountInfo);

    }

    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> accountStatement(@RequestBody AccountDto accountDto, @PathVariable String accountNumber) {
        List<Transaction> accountStatement;
        try {
            accountStatement = bankServices.generateAccountStatement(accountNumber, accountDto.getAccountPassword());

        } catch (BankAppException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(accountStatement, HttpStatus.OK);
    }

    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> accountInfo(@RequestBody AccountDto accountDto, @PathVariable String accountNumber) {
        AccountInfo accountinfo;
        try {
            accountinfo = bankServices.getAccountInfo(accountNumber, accountDto.getAccountPassword());

        } catch (BankAppException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new AccountInfoApiResponse(HttpStatus.OK, true, "successful", accountinfo), HttpStatus.OK);

    }




}
