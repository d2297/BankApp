package com.dami.bankapp.services;

import com.dami.bankapp.exceptions.BankAppException;
import com.dami.bankapp.model.Account;
import com.dami.bankapp.model.AccountDto;
import com.dami.bankapp.model.AccountInfo;
import com.dami.bankapp.model.DepositDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@Slf4j
public class BankServicesImplTest {

    @Autowired
    BankServicesImpl bankServices;
    AccountDto accountDto;

    @BeforeEach
    void setUp(){
        accountDto = new AccountDto();
        accountDto.setAccountName("Damilare Jolayemi");
        accountDto.setAccountPassword("password");
    }

    @AfterEach
    void tearDown(){
        accountDto = null;
        bankServices.resetRepo();
    }

    @Test
    void testToCreateAccount(){
        accountDto.setInitialDeposit(500.0);
        String message = null;
        try {
            message = bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        log.info("{}",message);
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");

        assertEquals(account.getAccountName(), accountDto.getAccountName());
        assertEquals(account.getPassword(), accountDto.getAccountPassword());
        assertEquals(account.getBalance(), accountDto.getInitialDeposit());
        assertEquals(10, account.getAccountNumber().length());

    }

    @Test
    void testToFindAccountByAccountNumber(){
        accountDto.setInitialDeposit(1000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");
        Account account1 = bankServices.findAccountByAccountNumber(account.getAccountNumber());
        assertEquals(account, account1);
    }

    @Test
    void testToDeposit(){
        accountDto.setInitialDeposit(1000.0);
        assertEquals(1000.0, accountDto.getInitialDeposit());
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");
        assertEquals(1000.0, account.getBalance());

        String message = bankServices.deposit(account.getAccountNumber(), 5000.0);
        log.info(message);
        assertEquals(6000.0, account.getBalance());
    }

    @Test
    void testToWithdraw(){
        accountDto.setInitialDeposit(10000.0);
        assertEquals(10000.0, accountDto.getInitialDeposit());
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");
        assertEquals(10000.0, account.getBalance());

        String message = bankServices.withdraw(account.getAccountNumber(), "password", 3000.0);
        log.info(message);
        assertEquals(7000.0, account.getBalance());
    }

    @Test
    void testToGenerateAccountStatement(){
        accountDto.setInitialDeposit(10000.0);
        assertEquals(10000.0, accountDto.getInitialDeposit());
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");
        assertEquals(10000.0, account.getBalance());
        assertEquals(1, account.getTransactions().size());

        bankServices.deposit(account.getAccountNumber(), 5000.0);
        bankServices.deposit(account.getAccountNumber(), 10000.0);
        bankServices.deposit(account.getAccountNumber(), 20000.0);
        bankServices.withdraw(account.getAccountNumber(), "password", 3000.0);
        bankServices.withdraw(account.getAccountNumber(), "password", 3000.0);

        log.info("Statement -----> {}", account.getTransactions());
        assertEquals(6, account.getTransactions().size());
        assertEquals(39000.0, account.getBalance());
    }

    @Test
    void testForAccountInfo(){
        accountDto.setInitialDeposit(10000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");
        accountDto.setAccountNumber(account.getAccountNumber());
       AccountInfo accountInfo = bankServices.getAccountInfo(accountDto.getAccountNumber(), accountDto.getAccountPassword());

        assertEquals(accountInfo.getAccountNumber(), account.getAccountNumber());
        assertEquals(accountInfo.getAccountName(), account.getAccountName());

    }

    @Test
    void testThatDepositAmountLessThanOneThrowsBankAppException(){
        accountDto.setInitialDeposit(10000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");

        assertThrows(BankAppException.class, ()-> bankServices.deposit(account.getAccountNumber(), -50.0));
    }

    @Test
    void testThatInvalidDepositAccountNumberThrowsBankAppException(){
        accountDto.setInitialDeposit(10000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }

        assertThrows(BankAppException.class, ()-> bankServices.deposit("1234", 5000.0));
    }

    @Test
    void testThatInvalidWithDrawAccountNumberThrowsBankAppException(){
        accountDto.setInitialDeposit(10000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }

        assertThrows(BankAppException.class, ()-> bankServices.withdraw("1234", "password",5000.0));
    }

    @Test
    void testThatInvalidWithDrawPasswordThrowsBankAppException(){
        accountDto.setInitialDeposit(10000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");


        assertThrows(BankAppException.class, ()-> bankServices.withdraw(account.getAccountNumber(), "psdf",5000.0));
    }

    @Test
    void testThatInvalidWithDrawAmountThrowsBankAppException(){
        accountDto.setInitialDeposit(1000.0);
        try {
            bankServices.createAccount(accountDto);
        } catch (BankAppException e) {
            e.printStackTrace();
        }
        Account account = bankServices.findAccountByAccountName("Damilare Jolayemi");


        assertThrows(BankAppException.class, ()-> bankServices.withdraw(account.getAccountNumber(), "password",700.0));
    }
}
