package com.dami.bankapp.services;

import com.dami.bankapp.exceptions.BankAppException;
import com.dami.bankapp.model.AccessToken;
import com.dami.bankapp.model.AccountDto;
import com.dami.bankapp.model.LoginDto;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class UserServicesImplTest {

    @Autowired
    UserServices userServices;
    @Autowired
    BankServices bankServices;

    AccountDto accountDto;
    LoginDto loginDto;

    @BeforeEach
    void setUp(){
        accountDto = new AccountDto();
        accountDto.setAccountName("Damilare Jolayemi");
        accountDto.setAccountPassword("password");
        accountDto.setInitialDeposit(1000.00);
        loginDto = new LoginDto();
    }


    @Test
    void testThatLoginGeneratesToken(){
        bankServices.createAccount(accountDto);
        String accountNumber = bankServices.findAccountByAccountName("Damilare Jolayemi").getAccountNumber();

        loginDto.setAccountNumber(accountNumber);
        loginDto.setPassword("password");
        AccessToken token = userServices.login(loginDto);
        log.info("Token --> {}", token);

        assertNotNull(token.getAccessToken());
    }

    @Test
    void testThatInvalidPassword_throwsBankAppException(){
        bankServices.createAccount(accountDto);
        String accountNumber = bankServices.findAccountByAccountName("Damilare Jolayemi").getAccountNumber();

        loginDto.setAccountNumber(accountNumber);
        loginDto.setPassword("slkdoin");
        assertThrows(BankAppException.class, ()-> userServices.login(loginDto));
    }


}