import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringOperationsTest {

    private StringOperations stringOperations;

    @BeforeEach
    void setUp() {
        stringOperations = new StringOperations();
    }

    @AfterEach
    void tearDown() {
        stringOperations = null;
    }

    @Test
    void testForAdditionOfSingleDigits(){
        String first = "2";
        String second = "2";
        String operator = "+";
        assertEquals("4", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForAdditionOfMultipleDigits(){
        String first = "123456789";
        String second = "987654321";
        String operator = "+";
        assertEquals("1111111110", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForAdditionOfNegativeNumberWithPositiveNumber(){
        String first = "-2";
        String second = "2";
        String operator = "+";
        assertEquals("0", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForAdditionOfTwoNegativeNumbers(){
        String first = "-150";
        String second = "-150";
        String operator = "+";
        assertEquals("-300", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForSubtractionOfSingleDigits(){
        String first = "5";
        String second = "2";
        String operator = "-";
        assertEquals("3", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForSubtractionOfMultipleDigits(){
        String first = "987654321";
        String second = "123456789";
        String operator = "-";
        assertEquals("864197532", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForSubtractionOfNegativeNumberWithPositiveNumber(){
        String first = "-30";
        String second = "20";
        String operator = "-";
        assertEquals("-50", stringOperations.calculate(first, second, operator));

        first = "-5";
        second = "2";
        assertEquals("-7", stringOperations.calculate(first, second, operator));

        first = "5";
        second = "-2";
        assertEquals("7", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForSubtractionOfTwoNegativeNumbers(){
       String first = "-5";
       String second = "-2";
       String operator = "-";
       assertEquals("-3", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForMultiplicationOfSingleDigits(){
        String first = "5";
        String second = "2";
        String operator = "*";
        assertEquals("10", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForMultiplicationOfMultipleDigits(){
        String first = "987654321";
        String second = "123456789";
        String operator = "*";
        assertEquals("121932631112635269", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForMultiplicationOfNegativeNumberWithPositiveNumber(){
        String first = "-30";
        String second = "20";
        String operator = "*";
        assertEquals("-600", stringOperations.calculate(first, second, operator));

        first = "-5";
        second = "2";
        assertEquals("-10", stringOperations.calculate(first, second, operator));

        first = "5";
        second = "-2";
        assertEquals("-10", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForMultiplicationOfTwoNegativeNumbers(){
        String first = "-5";
        String second = "-2";
        String operator = "*";
        assertEquals("10", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForDivisionOfSingleDigits(){
        String first = "8";
        String second = "2";
        String operator = "/";
        assertEquals("4", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForDivisionOfMultipleDigits(){
        String first = "987654321";
        String second = "123456789";
        String operator = "/";
        assertEquals("8", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForDivisionOfNegativeNumberWithPositiveNumber(){
        String first = "-30";
        String second = "20";
        String operator = "/";
        assertEquals("-1", stringOperations.calculate(first, second, operator));

        first = "-5";
        second = "2";
        assertEquals("-2", stringOperations.calculate(first, second, operator));

        first = "5";
        second = "-2";
        assertEquals("-2", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForDivisionOfTwoNegativeNumbers(){
        String first = "-5";
        String second = "-2";
        String operator = "/";
        assertEquals("2", stringOperations.calculate(first, second, operator));
    }

    @Test
    void testForZeroDenominatorInDivision_throwsIllegalArgumentException(){
        String first = "-5";
        String second = "0";
        String operator = "/";
        assertThrows(IllegalArgumentException.class, ()-> stringOperations.calculate(first, second, operator));
    }

    @Test
    void testThatInvalidOperator_throwsIllegalArgumentException(){
        String first = "-5";
        String second = "0";
        String operator = "^";
        assertThrows(IllegalArgumentException.class, ()-> stringOperations.calculate(first, second, operator));
    }


    @Test
    void testThatInvalidOperand_throwsIllegalArgumentException(){
        String first = "-j280";
        String second = "100";
        String operator = "+";
        assertThrows(IllegalArgumentException.class, ()-> stringOperations.calculate(first, second, operator));
    }

    @Test
    void testThatNullOperatorThrowsIllegalArgumentException(){
        String first = "-5";
        String second = "0";
        String operator = null;
        assertThrows(IllegalArgumentException.class, ()-> stringOperations.calculate(first, second, operator));
    }

    @Test
    void testThatNullOperandThrowsIllegalArgumentException(){
        assertThrows(IllegalArgumentException.class, ()-> stringOperations.calculate(null, null, "+"));

    }



}