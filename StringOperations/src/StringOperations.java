import java.math.BigInteger;

public class StringOperations {

    public String calculate(String first, String second, String operator){
        if(first == null || second == null){
            throw new IllegalArgumentException("Operand should comprise only digits and should start with '-' if it is less than zero");
        }

        String validOperators = "+-/*";
        if(operator == null || !validOperators.contains(operator)){
            throw new IllegalArgumentException("Invalid operator. Operator must be one of +, -, /, *");
        }

        BigInteger result = BigInteger.ZERO;
        BigInteger firstNumValue;
        BigInteger secondNumValue;

        if(first.startsWith("-")) {
            firstNumValue = extractNumValue(first.substring(1)).multiply(BigInteger.valueOf(-1));
        }else{
            firstNumValue = extractNumValue(first);
        }
        if(second.startsWith("-")) {
            secondNumValue = extractNumValue(second.substring(1)).multiply(BigInteger.valueOf(-1));
        }else{
            secondNumValue = extractNumValue(second);
        }

        if(operator.equals("/") && second.equals("0")){
            throw new IllegalArgumentException("Cannot divide by zero");
        }


        switch (operator) {
            case "+" -> result = firstNumValue.add(secondNumValue);
            case "-" -> result = firstNumValue.subtract(secondNumValue);
            case "*" -> result = firstNumValue.multiply(secondNumValue);
            case "/" -> result = firstNumValue.divide(secondNumValue);
        }


        return String.valueOf(result);
    }

    private BigInteger extractNumValue(String s){
        int counter = s.length() - 1;
        int multiplier = 1;

        while(counter > 0){
            multiplier *= 10;
            counter--;
        }
        int result = 0;

        for (int i = 0; i < s.length(); i++) {
            if(!Character.isDigit(s.charAt(i))){
                throw new IllegalArgumentException("Operand should comprise only digits and should start with '-' if it is less than zero");
            }
            result += Character.getNumericValue(s.charAt(i)) * multiplier;
            multiplier /= 10;
        }

        return BigInteger.valueOf(result);
    }

    public static void main(String[] args) {
        System.out.println(Character.getNumericValue('h'));
    }


}
