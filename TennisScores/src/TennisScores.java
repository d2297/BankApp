import java.util.Arrays;

public class TennisScores {

    public static int[] getResults(int[] firstPlayerScores, int[] secondPlayerScores){
        if(firstPlayerScores == null || secondPlayerScores == null){
            throw new IllegalArgumentException("scores must not be null");
        }
        if(firstPlayerScores.length != secondPlayerScores.length){
            throw new IllegalArgumentException("Number of scores for both players must be equal");
        }

        int[] result = new int[2];

        for (int i = 0; i < firstPlayerScores.length; i++) {
            if(firstPlayerScores[i] > secondPlayerScores[i]){
                result[0]++;
            }else if(firstPlayerScores[i] < secondPlayerScores[i]){
                result[1]++;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[] first = {4, 4, 4, 4, 4};
        int[] second = {1, 4, 4, 4, 4};
        System.out.println(Arrays.toString(TennisScores.getResults(first, second)));
    }
}
